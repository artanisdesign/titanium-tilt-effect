var win = Ti.UI.currentWindow;

var img = Ti.UI.createImageView({
	image : "http://farm7.staticflickr.com/6194/6156041431_e1f481a696.jpg",
	/* public domain photo from http://www.flickr.com/photos/65798313@N06/6156041431/sizes/m/in/pool-685365@N25/ */
	top : 60,
	width : 250,
	height : 187,
	left : 35
});

win.add(img);

img.addEventListener('touchstart', function(e) {
	
	tilt(e);

});
img.addEventListener('touchmove', function(e) {
	
	tilt(e);

});
img.addEventListener('touchend', function(e) {
	
	var t =  Titanium.UI.iOS.create3DMatrix();

	t = t.rotate(0,0,1,0);
	t = t.rotate(0,1,0,0);

	var a1 = Titanium.UI.createAnimation();
	a1.transform = t;
	a1.duration = 300;
	img.animate(a1);

	//img.transform = t;

});

function tilt(e){
	
	var t =  Titanium.UI.iOS.create3DMatrix();

	var rectW = img.getWidth();
	var rectH = img.getHeight();
	var maxDim = Math.max(rectW, rectH);

	var angle = 15;
	
	var distanceFromCenterX = -(rectW / 2) + e.x;
	var normalisedDistanceX = 2 * distanceFromCenterX / maxDim;

	var distanceFromCenterY = -(rectH / 2) + e.y;
	var normalisedDistanceY = 2 * distanceFromCenterY / maxDim;

	var tiltAngleX = normalisedDistanceX * angle;
	var tiltAngleY = - normalisedDistanceY * angle;


	t.m34 = 1.0/-1000;
	t = t.rotate(tiltAngleX,0,1,0);
	t = t.rotate(tiltAngleY,1,0,0);

	img.transform = t;
}
